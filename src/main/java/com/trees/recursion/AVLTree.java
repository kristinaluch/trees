package com.trees.recursion;
import com.trees.models.AVLNode;
import com.trees.interfaces.ITree;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;

public class AVLTree implements ITree {
    private int size;
    private AVLNode root;

    void updateHeight(AVLNode n) {
        n.height = 1 + Math.max(height(n.left), height(n.right));
    }

    int height(AVLNode n) {
        return n == null ? -1 : n.height;
    }

    int getBalance(AVLNode n) {
        return (n == null) ? 0 : height(n.right) - height(n.left);
    }

    AVLNode rotateRight(AVLNode y) {
        AVLNode x = y.left;
        AVLNode z = x.right;
        x.right = y;
        y.left = z;
        updateHeight(y);
        updateHeight(x);
        return x;
    }

    AVLNode rotateLeft(AVLNode y) {
        AVLNode x = y.right;
        AVLNode z = x.left;
        x.left = y;
        y.right = z;
        updateHeight(y);
        updateHeight(x);
        return x;
    }

    AVLNode rebalance(AVLNode z) {
        updateHeight(z);
        int balance = getBalance(z);
        if (balance > 1) {
            if (height(z.right.right) > height(z.right.left)) {
                z = rotateLeft(z);
            } else {
                z.right = rotateRight(z.right);
                z = rotateLeft(z);
            }
        } else if (balance < -1) {
            if (height(z.left.left) > height(z.left.right))
                z = rotateRight(z);
            else {
                z.left = rotateLeft(z.left);
                z = rotateRight(z);
            }
        }
        return z;
    }

    @Override
    public void add(int val)
    {
        try{
            root = add(root, val);
            size++;
        }
        catch (RuntimeException e) {
            //do later
        }
    }

    AVLNode add(AVLNode node, int val) {
        if (node == null) {
            return new AVLNode(val);
        } else if (node.item > val) {
            node.left = add(node.left, val);
        } else if (node.item < val) {
            node.right = add(node.right, val);
        } else {
            throw new RuntimeException("duplicate Key!");
        }
        return rebalance(node);
    }

    @Override
    public boolean del(int val)
    {
        try{
            root = delete(root, val);
            size--;
            return true;
        }
        catch (RuntimeException e) {
            //do later
            return false;
        }
    }

    AVLNode delete(AVLNode node, int val) {
        if (node == null) {
            throw new RuntimeException("no such Key!");
        } else if (node.item > val) {
            node.left = delete(node.left, val);
        } else if (node.item < val) {
            node.right = delete(node.right, val);
        } else {
            if (node.left == null || node.right == null) {
                node = (node.left == null) ? node.right : node.left;
            } else {
                AVLNode mostLeftChild = mostLeftChild(node.right);
                node.item = mostLeftChild.item;
                node.right = delete(node.right, node.item);
            }
        }
        if (node != null) {
            node = rebalance(node);
        }
        return node;
    }

    private AVLNode mostLeftChild(AVLNode node) {
        if (node.left == null) {
            return node;
        }
        else {
            return mostLeftChild(node.left);
        }
    }

    @Override
    public void init(int[] arr) {
        clear();
        for (int i = 0; i < arr.length; i++) {
            add(arr[i]);
        }
    }

    @Override
    public void print() {
        String stringArray = "[";
        int[] array = sortArrayBubble(toArray());
        for (int i = 0; i < array.length; i++) {
            stringArray += array[i];
            if (i != array.length - 1) {
                stringArray += ", ";
            }
        }
        stringArray += "]";
        System.out.println(stringArray);
    }

    @Override
    public void clear() {
        root = null;
        size = 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public int[] toArray() {
        ArrayList<AVLNode> arrayList = new ArrayList<>();

        if (root == null) {
            return new int[0];
        }

        fillArray(root, arrayList);

        int[] array = new int[arrayList.size()];

        Iterator<AVLNode> iterator = arrayList.iterator();

        for (int i = 0; i < array.length; i++) {
            AVLNode current = iterator.next();
            array[i] = current.item;
        }

        return array;
    }

    private void fillArray(AVLNode node, ArrayList<AVLNode> arrayList) {

        if (node.left != null) {
            fillArray(node.left, arrayList);
        }

        arrayList.add(node);

        if (node.right != null) {
            fillArray(node.right, arrayList);
        }
    }

    @Override
    public int getWidth() {
        if (root == null) {
            return 0;
        }

        int levels = getHeight();
        int[] levelsWidth = new int[levels];
        countLevel(1, root, levelsWidth);
        int maxSize = levelsWidth[0];

        for (int i = 1; i < levelsWidth.length; i++) {
            if (levelsWidth[i] > maxSize) {
                maxSize = levelsWidth[i];
            }
        }

        return maxSize;
    }

    public void countLevel(int level, AVLNode node, int[] levelsWidth) {

        if (node.left != null) {
            countLevel(level + 1, node.left, levelsWidth);
        }

        if (node.right != null) {
            countLevel(level + 1, node.right, levelsWidth);
        }

        levelsWidth[level - 1] += 1;
    }

    @Override
    public int getHeight() {
        return getHeight(root);
    }

    private int getHeight(AVLNode node) {
        if (node == null) {
            return 0;
        }

        int height = 1 + Math.max(getHeight(node.left), getHeight(node.right));
        return height;
    }

    @Override
    public int nodes() {
        int nodesNotLeaves = size - leaves();
        return nodesNotLeaves;
    }

    @Override
    public int leaves() {
        if (root == null) {
            return 0;
        }

        return leaves(root);
    }

    private int leaves(AVLNode current) {
        if (current.left == null && current.right == null) {
            return 1;
        }

        int sumHeirs = 0;

        if (current.left != null) {
            sumHeirs += leaves(current.left);
        }

        if (current.right != null) {
            sumHeirs += leaves(current.right);
        }

        return sumHeirs;
    }

    @Override
    public void reverse() {
        if (root != null) {
            reverse(root);
        }
    }

    private void reverse(AVLNode node) {
        if (node.left != null) {
            reverse(node.left);
        }

        if (node.right != null) {
            reverse(node.right);
        }

        if (node.left != null || node.right != null) {
            AVLNode temp = node.left;
            node.left = node.right;
            node.right = temp;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AVLTree avlTree = (AVLTree) o;
        return size == avlTree.size && Objects.equals(root, avlTree.root);
    }

    @Override
    public int hashCode() {
        return Objects.hash(size, root);
    }

    private int[] sortArrayBubble(int[] array){
        boolean notSorted = true;
        while (notSorted) {
            notSorted = false;
            for(int i = 1; i < array.length; i++) {
                if (array[i] < array[i - 1]) {
                    int x = array[i];
                    array[i] = array[i - 1];
                    array[i - 1] = x;
                    notSorted = true;
                }
            }
        }
        return array;
    }
}
