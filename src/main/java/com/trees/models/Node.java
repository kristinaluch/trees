package com.trees.models;

import java.util.Objects;

public class Node{
    public int item;
    public Node left;
    public Node right;

    public Node(int item){
        this.item = item;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Node node = (Node) o;
        return item == node.item && Objects.equals(left, node.left) && Objects.equals(right, node.right);
    }

    @Override
    public int hashCode() {
        return Objects.hash(item, left, right);
    }

    public static Node deepCopy(Node node) {
        if (node == null) {
            return null;
        }
        Node newNode = new Node(node.item);
        if (node.left != null) {
            newNode.left = deepCopy(node.left);
        }
        if (node.right != null) {
            newNode.right = deepCopy(node.right);
        }
        return newNode;
    }
}