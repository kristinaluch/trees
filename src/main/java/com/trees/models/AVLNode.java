package com.trees.models;

import java.util.Objects;

public class AVLNode {
    public int item;
    public int height;
    public AVLNode left;
    public AVLNode right;

    public AVLNode(int item) {
        this.item = item;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AVLNode avlNode = (AVLNode) o;
        return item == avlNode.item && Objects.equals(left, avlNode.left) && Objects.equals(right, avlNode.right);
    }

    @Override
    public int hashCode() {
        return Objects.hash(item, height, left, right);
    }

    public static AVLNode deepCopy(AVLNode node) {
        if (node == null) {
            return null;
        }

        AVLNode newNode = new AVLNode(node.item);
        newNode.height = node.height;

        if (node.left != null) {
            newNode.left = deepCopy(node.left);
        }
        if (node.right != null) {
            newNode.right = deepCopy(node.right);
        }
        return newNode;
    }
}
