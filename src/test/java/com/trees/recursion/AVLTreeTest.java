package com.trees.recursion;

import com.trees.models.AVLNode;
import com.trees.recursionTestData.AVLTreeTestData;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.lang.reflect.Field;


public class AVLTreeTest {
    AVLTree cut = new AVLTree();

    static Arguments[] addTestArgs() {
        return new Arguments[] {
                Arguments.arguments(AVLNode.deepCopy(AVLTreeTestData.ROOT1), 0, 47, AVLTreeTestData.addTreeTest1),
                Arguments.arguments(AVLNode.deepCopy(AVLTreeTestData.ROOT2), 5, 115, AVLTreeTestData.addTreeTest2)
        };
    }

    @ParameterizedTest
    @MethodSource("addTestArgs")
    void addTest(AVLNode reflect, int reflectSize, int value, AVLTree expected) throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("root");
        field1.setAccessible(true);
        field1.set(cut, reflect);

        Field field2 = cut.getClass().getDeclaredField("size");
        field2.setAccessible(true);
        field2.set(cut, reflectSize);

        cut.add(value);

        Assertions.assertEquals(expected, cut);
    }

    static Arguments[] delTestArgs() {
        return new Arguments[] {
                Arguments.arguments(AVLNode.deepCopy(AVLTreeTestData.ROOT2), 5, 30, AVLTreeTestData.deleteTreeTest1, true),
                Arguments.arguments(AVLNode.deepCopy(AVLTreeTestData.ROOT2), 5, 150, AVLTreeTestData.deleteTreeTest2, true)
        };
    }

    @ParameterizedTest
    @MethodSource("delTestArgs")
    void delTest(AVLNode reflect, int reflectSize, int value, AVLTree expected, boolean boolExpected)
            throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("root");
        field1.setAccessible(true);
        field1.set(cut, reflect);

        Field field2 = cut.getClass().getDeclaredField("size");
        field2.setAccessible(true);
        field2.set(cut, reflectSize);

        boolean actual = cut.del(value);

        Assertions.assertEquals(expected, cut);
        Assertions.assertEquals(boolExpected, actual);
    }

    static Arguments[] clearTestArgs() {
        return new Arguments[] {
                Arguments.arguments(AVLNode.deepCopy(AVLTreeTestData.ROOT1), 0, AVLTreeTestData.originalTreeTest1),
                Arguments.arguments(AVLNode.deepCopy(AVLTreeTestData.ROOT2), 5, AVLTreeTestData.originalTreeTest1)
        };
    }

    @ParameterizedTest
    @MethodSource("clearTestArgs")
    void clearTest(AVLNode reflect, int reflectSize, AVLTree expected) throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("root");
        field1.setAccessible(true);
        field1.set(cut, reflect);

        Field field2 = cut.getClass().getDeclaredField("size");
        field2.setAccessible(true);
        field2.set(cut, reflectSize);

        cut.clear();

        Assertions.assertEquals(expected, cut);
    }

    static Arguments[] sizeTestArgs() {
        return new Arguments[] {
                Arguments.arguments(AVLNode.deepCopy(AVLTreeTestData.ROOT1), 0, 23, 1),
                Arguments.arguments(AVLNode.deepCopy(AVLTreeTestData.ROOT2), 5, 45, 6)
        };
    }

    @ParameterizedTest
    @MethodSource("sizeTestArgs")
    void sizeTest(AVLNode reflect, int reflectSize, int numAdd, int expected) throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("root");
        field1.setAccessible(true);
        field1.set(cut, reflect);

        Field field2 = cut.getClass().getDeclaredField("size");
        field2.setAccessible(true);
        field2.set(cut, reflectSize);

        cut.add(numAdd);
        int actual = cut.size();
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] toArrayTestArgs() {
        return new Arguments[] {
                Arguments.arguments(AVLNode.deepCopy(AVLTreeTestData.ROOT1), 0, new int[] {}),
                Arguments.arguments(AVLNode.deepCopy(AVLTreeTestData.ROOT2), 5, new int[] {30, 50, 100, 125, 150})
        };
    }

    @ParameterizedTest
    @MethodSource("toArrayTestArgs")
    void toArrayTest(AVLNode reflect, int reflectSize, int[] expected) throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("root");
        field1.setAccessible(true);
        field1.set(cut, reflect);

        Field field2 = cut.getClass().getDeclaredField("size");
        field2.setAccessible(true);
        field2.set(cut, reflectSize);

        int[] actual = cut.toArray();

        Assertions.assertArrayEquals(expected, actual);
    }

    static Arguments[] initTestArgs() {
        return new Arguments[] {
                Arguments.arguments(AVLNode.deepCopy(AVLTreeTestData.ROOT_REVERSE1), 0, new int[] {}, AVLTreeTestData.originalTreeTest1),
                Arguments.arguments(AVLNode.deepCopy(AVLTreeTestData.ROOT_REVERSE1), 5, new int[] {100, 50, 150, 30, 125}, AVLTreeTestData.originalTreeTest2),
        };
    }

    @ParameterizedTest
    @MethodSource("initTestArgs")
    void initTest(AVLNode reflect, int reflectSize, int[] array, AVLTree expected) throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("root");
        field1.setAccessible(true);
        field1.set(cut, reflect);

        Field field2 = cut.getClass().getDeclaredField("size");
        field2.setAccessible(true);
        field2.set(cut, reflectSize);

        cut.init(array);

        Assertions.assertEquals(expected, cut);
    }

    static Arguments[] nodesTestArgs() {
        return new Arguments[] {
                Arguments.arguments(AVLNode.deepCopy(AVLTreeTestData.ROOT1), 0, 0),
                Arguments.arguments(AVLNode.deepCopy(AVLTreeTestData.ROOT2), 5, 3)
        };
    }

    @ParameterizedTest
    @MethodSource("nodesTestArgs")
    void nodesTest(AVLNode reflect, int reflectSize, int expected) throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("root");
        field1.setAccessible(true);
        field1.set(cut, reflect);

        Field field2 = cut.getClass().getDeclaredField("size");
        field2.setAccessible(true);
        field2.set(cut, reflectSize);

        int actual = cut.nodes();
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] leavesTestArgs() {
        return new Arguments[] {
                Arguments.arguments(AVLNode.deepCopy(AVLTreeTestData.ROOT1), 0, 0),
                Arguments.arguments(AVLNode.deepCopy(AVLTreeTestData.ROOT2), 5, 2)
        };
    }

    @ParameterizedTest
    @MethodSource("leavesTestArgs")
    void leavesTest(AVLNode reflect, int reflectSize, int expected) throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("root");
        field1.setAccessible(true);
        field1.set(cut, reflect);

        Field field2 = cut.getClass().getDeclaredField("size");
        field2.setAccessible(true);
        field2.set(cut, reflectSize);

        int actual = cut.leaves();
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] getWidthTestArgs() {
        return new Arguments[] {
                Arguments.arguments(AVLNode.deepCopy(AVLTreeTestData.ROOT1), 0, 0),
                Arguments.arguments(AVLNode.deepCopy(AVLTreeTestData.ROOT2), 5, 2)
        };
    }

    @ParameterizedTest
    @MethodSource("getWidthTestArgs")
    void getWidthTest(AVLNode reflect, int reflectSize,  int expected) throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("root");
        field1.setAccessible(true);
        field1.set(cut, reflect);

        Field field2 = cut.getClass().getDeclaredField("size");
        field2.setAccessible(true);
        field2.set(cut, reflectSize);

        int actual = cut.getWidth();
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] getHeightTestArgs() {
        return new Arguments[] {
                Arguments.arguments(AVLNode.deepCopy(AVLTreeTestData.ROOT1), 0, 0),
                Arguments.arguments(AVLNode.deepCopy(AVLTreeTestData.ROOT2), 5, 3)
        };
    }

    @ParameterizedTest
    @MethodSource("getHeightTestArgs")
    void getHeightTest(AVLNode reflect, int reflectSize,  int expected) throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("root");
        field1.setAccessible(true);
        field1.set(cut, reflect);

        Field field2 = cut.getClass().getDeclaredField("size");
        field2.setAccessible(true);
        field2.set(cut, reflectSize);

        int actual = cut.getHeight();
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] reverseTestArgs() {
        return new Arguments[] {
                Arguments.arguments(AVLNode.deepCopy(AVLTreeTestData.ROOT1), 0, AVLTreeTestData.reverseTreeTest1),
                Arguments.arguments(AVLNode.deepCopy(AVLTreeTestData.ROOT2), 5, AVLTreeTestData.reverseTreeTest2)
        };
    }

    @ParameterizedTest
    @MethodSource("reverseTestArgs")
    void reverseTest(AVLNode reflect, int reflectSize, AVLTree expected)
            throws NoSuchFieldException, IllegalAccessException {
        Field field1 = cut.getClass().getDeclaredField("root");
        field1.setAccessible(true);
        field1.set(cut, reflect);

        Field field2 = cut.getClass().getDeclaredField("size");
        field2.setAccessible(true);
        field2.set(cut, reflectSize);

        cut.reverse();

        Assertions.assertEquals(expected, cut);
    }
}
